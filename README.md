# Very simple file system

A very simple file system based on the initial unix fs.

## Goals of this project
In this project, I will create a program to emulate a very simple file system.
Description of said file system can be gotten from here
https://dsf.berkeley.edu/cs262/FFS-annotated.pdf.

- [ ] Create a virtual storage block (array of size 150 megabytes)
- [ ] Implement an API to open, read, write, append, close files
- [ ] Implement API to create, remove directories
- [ ] Implement API to add files to directories
- [ ] Implement a "SHELL" interface to interact with this filesystem
